# Remove LINGUAS from dependecy list in ./po/Makefile
/^Makefile[ 	]*:/ s/LINGUAS$/#&/

# Adjust testsuite path in ./tests/Makefile
/^TESTS_ENVIRONMENT[ 	]*=[ 	]*UNRTF/ s/builddir/srcdir/2
